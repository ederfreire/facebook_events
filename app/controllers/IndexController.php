<?php

class IndexController extends Phalcon\Mvc\Controller 
{

	public function indexAction()
	{
		$config = array(
			'appId' => $this->config->facebookapp->appId,
			'secret' => $this->config->facebookapp->secret,
			'allowSignedRequest' => false,
			'scope' => 'user_events',
		);

		$facebook = new \Facebook($config);
		$user_id = $facebook->getUser();

		if($user_id == 0) {
			$this->view->setVar("login_url", $facebook->getLoginUrl());
		} else {
			$this->view->setVar("logout_url", $facebook->getLogoutUrl());

			$user = $facebook->api('/me','GET');
			$this->view->setVar("user", $user);

			$fbevents = $facebook->api('/me/events', 'GET');

			if($fbevents) {
				foreach($fbevents['data'] as $e){
					$event = \Events::findFirst(array(
						array("fb_id" => $e['id'])
					));
					if(!$event){
						$event = new \Events();
					}
					$event->fb_id = $e['id'];
					$event->user = $user['id'];
					$event->name = $e['name'];
					$event->location = $e['location'];
					$event->start_time = $e['start_time'];
					$event->status = $e['rsvp_status'];

					$event->save();
				}
				$events = \Events::find(array(
					"sort" => array("start_time" => -1),
					"limit" => 100,
				));
				$this->view->setVar("events", $events);
				$rvsp = array('attending'=>'Vou','declined'=>'Não vou','maybe'=>'Talvez','invited'=>'Convidado');
				$this->view->setVar("rvsp",$rvsp);
			}
		}

	}

}
