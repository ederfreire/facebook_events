<?php
#ini_set('display_errors',1);
#ini_set('display_startup_errors',1);
#error_reporting(-1);

require '../library/facebook.php';

$loader = new Phalcon\Loader();
$loader->registerDirs(array('models/'))->register();

try {

    $config = new Phalcon\Config\Adapter\Ini('../app/config/config.ini.php');

    //Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/',
        '../app/libraries/'
    ))->register();

    //Create a DI
    $di = new Phalcon\DI\FactoryDefault();

    $di->setShared('config', $config);

    //Setup the view component
    $di->set('view', function(){
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    //Setup a base URI so that all generated URIs include the "tutorial" folder
    $di->set('url', function(){
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('/events/');
        return $url;
    });


    $di->set('mongo', function() {
        $mongo = new Mongo();
        return $mongo->selectDb("facebook_events");
    }, true);

    $di->set('collectionManager', function(){
        return new Phalcon\Mvc\Collection\Manager();
    }, true);

    //Handle the request
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch(\Phalcon\Exception $e) {
     echo "PhalconException: ", $e->getMessage();
}