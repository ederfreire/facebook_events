Facebook Events
================
Este script foi desenvolvido para participação de um desafio de programação, ele pode ser utilizado para consultas.


Get Started
-----------

#### Requirements

To run this application on your machine, you need at least:

* PHP >= 5.3.11
* Apache Web Server with mod rewrite enabled
* Phalcon PHP Framework extension enabled (0.5.x)
* MongoDB 2.6 >=

